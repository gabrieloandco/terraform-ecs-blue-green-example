export app_version=2
export image_tag="latest"
export repository_url="802295647881.dkr.ecr.us-east-2.amazonaws.com/nginx-image"
export repo_id="802295647881"
export region="us-east-2"
aws ecr get-login-password --region ${region} \
| docker login --username AWS --password-stdin ${repo_id}.dkr.ecr.${region}.amazonaws.com \
&& docker build ./ -t ${repository_url}:${image_tag} --build-arg VERSION=${app_version} \
 && docker push  ${repository_url}:${image_tag}

aws deploy create-deployment --application-name ecs-example --deployment-group-name ecs-example --revision '{"revisionType": "S3", "s3Location": {"bucket":"codepipeline-8c773cea", "key":"app.yaml", "bundleType":"YAML"} }'    