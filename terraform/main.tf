terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "us-east-2"
}

resource "aws_ecr_repository" "ecs_example" {
  name                 = "nginx-image"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "null_resource" "push_docker_image" {
  provisioner "local-exec" {
    command = "aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin ${aws_ecr_repository.ecs_example.repository_url} && docker build ../ -t ${aws_ecr_repository.ecs_example.repository_url}:${var.image_tag} --build-arg VERSION=${var.app_version} && docker push  ${aws_ecr_repository.ecs_example.repository_url}:${var.image_tag}"
  }

    depends_on = [
      aws_ecr_repository.ecs_example
  ]
}

resource "aws_ecs_cluster" "ecs_example" {
  name = "ecs-example"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_subnet" "defaulta" {
  availability_zone = "us-east-2a"

  tags = {
    Name = "Default subnet for us-east-2a"
  }
}

resource "aws_default_subnet" "defaultb" {
  availability_zone = "us-east-2b"

  tags = {
    Name = "Default subnet for us-east-2a"
  }
}

resource "aws_iam_role" "ecs_role" {
  name = "ecs-role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
})
}

resource "aws_iam_role_policy_attachment" "ecs_role" {
  role       = aws_iam_role.ecs_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_ecs_task_definition" "ecs_example" {
  family = "service"
  network_mode = "awsvpc"
  cpu = "256"
  memory  = "1024"
  execution_role_arn = aws_iam_role.ecs_role.arn
  container_definitions = jsonencode([
    {
      name      = var.container_name
      image     = "${aws_ecr_repository.ecs_example.repository_url}:${var.image_tag}"
      essential = true
      portMappings = [
        { protocol = "tcp"
          containerPort = 80
          hostPort      =  80
        }
      ]
    }
  ])

  requires_compatibilities = ["FARGATE"]
}


resource "aws_security_group" "ecs_example" {
  name        = "allow_HTTP"
  description = "Allow HTTP inbound traffic"
  vpc_id      = aws_default_vpc.default.id

  ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_http"
  }
}

resource "aws_lb" "ecs_example" {
  name               = "ecs-example"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.ecs_example.id]
  subnets            = [aws_default_subnet.defaulta.id,aws_default_subnet.defaultb.id ]

  enable_deletion_protection = false

}

resource "aws_lb_target_group" "ecs_example_green" {
  name     = "ecs-example-green"
  port     = 80
  protocol = "HTTP"
  target_type = "ip"
  vpc_id   = aws_default_vpc.default.id

  depends_on = [
    aws_lb.ecs_example
  ]
 
  health_check {
    path = "/"
    port = 80
    healthy_threshold = 2
    unhealthy_threshold = 6
    timeout = 5
    interval = 10
    matcher = "200" 
  }

}

resource "aws_lb_listener" "ecs_example" {
  load_balancer_arn = aws_lb.ecs_example.arn
  port = "80"
  protocol = "HTTP"

  depends_on = [
    aws_lb.ecs_example, 
    aws_lb_target_group.ecs_example_green
  ]

  default_action {
    target_group_arn = aws_lb_target_group.ecs_example_green.arn
    type = "forward"
  }

}


resource "aws_ecs_service" "ecs_example" {
  name            = "ecs-example"
  cluster         = aws_ecs_cluster.ecs_example.id
  task_definition = aws_ecs_task_definition.ecs_example.arn
  desired_count   = 3
  launch_type = "FARGATE"

depends_on = [
    aws_lb_target_group.ecs_example_green,
    aws_ecs_task_definition.ecs_example
  ]

  network_configuration{
    security_groups = [aws_security_group.ecs_example.id]
    subnets = [aws_default_subnet.defaulta.id,aws_default_subnet.defaultb.id ]
    assign_public_ip = true #CHANGE
  }


  load_balancer {
    target_group_arn = aws_lb_target_group.ecs_example_green.arn
    container_name   = var.container_name
    container_port   = 80
  }

  deployment_controller  {
    type = "CODE_DEPLOY"
  }

  lifecycle {
    ignore_changes = [task_definition, desired_count]
 }

}


resource "aws_codedeploy_app" "ecs_example" {
  compute_platform = "ECS"
  name             = "ecs-example"
}

resource "aws_lb_target_group" "ecs_example_blue" {
  name     = "blue-ecs-example"
  port     = 80
  protocol = "HTTP"
  target_type = "ip"
  vpc_id   = aws_default_vpc.default.id

  depends_on = [
    aws_lb.ecs_example
  ]

  health_check {
    path = "/"
    port = 80
    healthy_threshold = 2
    unhealthy_threshold = 6
    timeout = 5
    interval = 10
    matcher = "200" 
  }


}


resource "aws_iam_role" "code_deploy_role" {
  name = "code-deploy-role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Service": [
                    "codedeploy.amazonaws.com"
                ]
            },
            "Action": "sts:AssumeRole"
        }
    ]
})
}

resource "aws_iam_role_policy_attachment" "code_deploy_role" {
  role       = aws_iam_role.code_deploy_role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSCodeDeployRoleForECS"
}

resource "aws_codedeploy_deployment_config" "ecs_example" {
  deployment_config_name = "Traffic_Control"
  compute_platform       = "ECS"

  traffic_routing_config {
    type = "TimeBasedCanary"

    time_based_canary {
      interval   = var.traffic_control_minutes
      percentage = var.traffic_control_percentage
    }
  }
}

resource "aws_codedeploy_deployment_group" "ecs_example"  {
  app_name               = aws_codedeploy_app.ecs_example.name
  deployment_config_name = var.traffic_control== "enabled" ? aws_codedeploy_deployment_config.ecs_example.deployment_config_name: "CodeDeployDefault.ECSAllAtOnce"
  deployment_group_name  = "ecs-example"
  service_role_arn       = aws_iam_role.code_deploy_role.arn

  depends_on = [
    aws_lb_target_group.ecs_example_green,
    aws_ecs_cluster.ecs_example,
    aws_ecs_service.ecs_example
    ]

  load_balancer_info {
    target_group_pair_info {
      prod_traffic_route {
        listener_arns = [aws_lb_listener.ecs_example.arn]
      }

      target_group {
        name = aws_lb_target_group.ecs_example_blue.name
      }

      target_group {
        name = aws_lb_target_group.ecs_example_green.name
      }
    }
  }

  ecs_service {
    cluster_name = aws_ecs_cluster.ecs_example.name
    service_name = aws_ecs_service.ecs_example.name
  }


  blue_green_deployment_config {
    deployment_ready_option {
      action_on_timeout = "CONTINUE_DEPLOYMENT"
    }

    terminate_blue_instances_on_deployment_success {
      action                           = "TERMINATE"
      termination_wait_time_in_minutes = 5
    }
  }

  deployment_style {
    deployment_option = "WITH_TRAFFIC_CONTROL"
    deployment_type   = "BLUE_GREEN"
  }
}


resource "aws_s3_bucket" "codepipeline" {
  bucket = "codepipeline-8c773cea"
  acl    = "private"
  versioning  {
    enabled    = true
    mfa_delete = false
  }

}


resource "null_resource" "create_upload_code" {
  provisioner "local-exec" {
    command = <<EOF
    echo '
version: 0.0
Resources:
  - TargetService:
      Type: AWS::ECS::Service
      Properties:
        TaskDefinition: ${aws_ecs_task_definition.ecs_example.arn}
        LoadBalancerInfo:
          ContainerName: ${var.container_name}
          ContainerPort: 80 ' > ./aws_codes/app.yaml && aws s3 cp ./aws_codes/app.yaml s3://${aws_s3_bucket.codepipeline.id}
    EOF
  }
}


##CodePipeline Doesnt work for Now

data "null_data_source" "task_definition" {
  inputs = {
    json = <<EOF
{
        "containerDefinitions": [
            {
                "name":  "${var.container_name}",
                "image": "${aws_ecr_repository.ecs_example.repository_url}:${var.image_tag}",
                "cpu": 0,
                "portMappings": [
                    {
                        "containerPort": 80,
                        "hostPort": 80,
                        "protocol": "tcp"
                    }
                ],
                "essential": true
            }
        ],
        "family": "service",
        "executionRoleArn": "${aws_iam_role.ecs_role.arn}",
        "networkMode": "awsvpc",
        "volumes": [],
        "compatibilities": [
            "EC2",
            "FARGATE"
        ],
        "requiresCompatibilities": [
            "FARGATE"
        ],
        "cpu": "256",
        "memory": "1024"
}
EOF
}
}
resource "null_resource" "create_upload_task_definition" {
  provisioner "local-exec" {
    command = "echo '${data.null_data_source.task_definition.outputs["json"]}' >  ./aws_codes/taskdef.json && aws s3 cp ./aws_codes/taskdef.json s3://${aws_s3_bucket.codepipeline.id}"
  }
}

resource "null_resource" "create_upload_appsec" {
  provisioner "local-exec" {
    command = <<EOF
    echo '
version: 0.0
Resources:
  - TargetService:
      Type: AWS::ECS::Service
      Properties:
        LoadBalancerInfo:
          ContainerName: ${var.container_name}
          ContainerPort: 80 ' > ./aws_codes/appsec.yaml && aws s3 cp ./aws_codes/appsec.yaml s3://${aws_s3_bucket.codepipeline.id}
    EOF
  }
}


resource "aws_iam_role" "codepipeline_role" {
  name = "codepipeline_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "codepipeline.amazonaws.com"
        }
      },
    ]
  })

}

resource "aws_iam_policy" "codepipeline_policy" {
  name        = "codepipeline_policy"
  policy      = jsonencode({
    "Statement": [
        {
            "Action": [
                "iam:PassRole"
            ],
            "Resource": "*",
            "Effect": "Allow",
            "Condition": {
                "StringEqualsIfExists": {
                    "iam:PassedToService": [
                        "cloudformation.amazonaws.com",
                        "elasticbeanstalk.amazonaws.com",
                        "ec2.amazonaws.com",
                        "ecs-tasks.amazonaws.com"
                    ]
                }
            }
        },
        {
            "Action": [
                "codecommit:CancelUploadArchive",
                "codecommit:GetBranch",
                "codecommit:GetCommit",
                "codecommit:GetRepository",
                "codecommit:GetUploadArchiveStatus",
                "codecommit:UploadArchive"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Action": [
                "codedeploy:CreateDeployment",
                "codedeploy:GetApplication",
                "codedeploy:GetApplicationRevision",
                "codedeploy:GetDeployment",
                "codedeploy:GetDeploymentConfig",
                "codedeploy:RegisterApplicationRevision"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Action": [
                "codestar-connections:UseConnection"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Action": [
                "elasticbeanstalk:*",
                "ec2:*",
                "elasticloadbalancing:*",
                "autoscaling:*",
                "cloudwatch:*",
                "s3:*",
                "sns:*",
                "cloudformation:*",
                "rds:*",
                "sqs:*",
                "ecs:*"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Action": [
                "lambda:InvokeFunction",
                "lambda:ListFunctions"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Action": [
                "opsworks:CreateDeployment",
                "opsworks:DescribeApps",
                "opsworks:DescribeCommands",
                "opsworks:DescribeDeployments",
                "opsworks:DescribeInstances",
                "opsworks:DescribeStacks",
                "opsworks:UpdateApp",
                "opsworks:UpdateStack"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Action": [
                "cloudformation:CreateStack",
                "cloudformation:DeleteStack",
                "cloudformation:DescribeStacks",
                "cloudformation:UpdateStack",
                "cloudformation:CreateChangeSet",
                "cloudformation:DeleteChangeSet",
                "cloudformation:DescribeChangeSet",
                "cloudformation:ExecuteChangeSet",
                "cloudformation:SetStackPolicy",
                "cloudformation:ValidateTemplate"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Action": [
                "codebuild:BatchGetBuilds",
                "codebuild:StartBuild",
                "codebuild:BatchGetBuildBatches",
                "codebuild:StartBuildBatch"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Effect": "Allow",
            "Action": [
                "devicefarm:ListProjects",
                "devicefarm:ListDevicePools",
                "devicefarm:GetRun",
                "devicefarm:GetUpload",
                "devicefarm:CreateUpload",
                "devicefarm:ScheduleRun"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "servicecatalog:ListProvisioningArtifacts",
                "servicecatalog:CreateProvisioningArtifact",
                "servicecatalog:DescribeProvisioningArtifact",
                "servicecatalog:DeleteProvisioningArtifact",
                "servicecatalog:UpdateProduct"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "cloudformation:ValidateTemplate"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ecr:DescribeImages"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "states:DescribeExecution",
                "states:DescribeStateMachine",
                "states:StartExecution"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "appconfig:StartDeployment",
                "appconfig:StopDeployment",
                "appconfig:GetDeployment"
            ],
            "Resource": "*"
        }
    ],
    "Version": "2012-10-17"
})
}

resource "aws_iam_policy_attachment" "codepipeline_attach" {
  name       = "codepipeline_attachment"
  roles      = [aws_iam_role.codepipeline_role.name]
  policy_arn = aws_iam_policy.codepipeline_policy.arn
}

resource "aws_iam_policy_attachment" "code_deploy_attach" {
  name       = "code_deploy_attachment"
  roles      = [aws_iam_role.code_deploy_role.name]
  policy_arn = aws_iam_policy.codepipeline_policy.arn
}



resource "aws_codepipeline" "codepipeline" {
  name     = "ecs_example"
  role_arn = aws_iam_role.codepipeline_role.arn

  artifact_store {
    location = aws_s3_bucket.codepipeline.bucket
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "ECR"
      version          = "1"
      output_artifacts = ["ImageArtifact"]

      configuration = {
        ImageTag = var.image_tag
        RepositoryName = aws_ecr_repository.ecs_example.name
        
      }
    }

    action {
      name             = "SourceAppSec"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version          = "1"
      output_artifacts = ["SourceAppSec"]

      configuration = {
        S3Bucket =  aws_s3_bucket.codepipeline.bucket
        S3ObjectKey = "appsec.yaml"
      }
    }

    action {
      name             = "SourceTaskDef"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version          = "1"
      output_artifacts = ["SourceTaskDef"]

      configuration = {
        S3Bucket =  aws_s3_bucket.codepipeline.bucket
        S3ObjectKey = "taskdef.json"
      }
    }
  }

  stage {
  name = "Approve"

  action {
    name     = "Approval"
    category = "Approval"
    owner    = "AWS"
    provider = "Manual"
    version  = "1"
  }
  }

  stage {
    name = "Deploy"

    action {
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "CodeDeployToECS"
      input_artifacts = ["SourceAppSec","SourceTaskDef","ImageArtifact"]
      version         = "1"


      configuration = {
        ApplicationName = aws_codedeploy_app.ecs_example.name
        DeploymentGroupName = aws_codedeploy_deployment_group.ecs_example.deployment_group_name
        AppSpecTemplateArtifact = "SourceAppSec"
        AppSpecTemplatePath =  "appspec.yaml"
        TaskDefinitionTemplateArtifact = "SourceTaskDef"
        TaskDefinitionTemplatePath = "taskdef.json"
        # Image1ArtifactName = "ImageArtifact"
        # Image1ContainerName = "${aws_ecr_repository.ecs_example.repository_url}:${var.image_tag}"
      }
    }
  }
}
