output "lb_dns" {
  value = aws_lb.ecs_example.dns_name
}

output "ecr_repository_url" {
  value = aws_ecr_repository.ecs_example.repository_url
}

output "ecr_repository_id" {
  value = aws_ecr_repository.ecs_example.registry_id
}

output "image_tag" {
  value = var.image_tag
}
