variable "container_name" {
  type = string
  default = "nginx"
}


variable "app_version" {
  type = string
  default = "1"
}

variable "image_tag" {
  type = string
  default = "latest"
}


variable "traffic_control" {
  type = string
  default = "enabled"
}


variable "traffic_control_minutes" {
  type = number
  default = 1
}

variable "traffic_control_percentage" {
  type = number
  default = 10
}