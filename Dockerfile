ARG VERSION=1
FROM nginx:1.21.0
ARG VERSION
COPY ./nginx.confv${VERSION} /etc/nginx/conf.d/default.conf
CMD ["/bin/bash", "-c", "nginx -g \"daemon off;\""]