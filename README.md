### AWS Blue Green Deployment - DevOps Challenge

### Overview
A deployment strategy will be implemented using AWS-Services that allows routing traffic from one server to another without any downtime.
The routing should be controllable via script. Servers can simply be `NGINX`s responding with different versions or names, e.g:

```
$ ./deploy-infrastructure # optional if it is not already setup.
$
$ for i in `seq 1 5`; do curl $DESTINATION; done
Hi, I am Frank
Hi, I am Frank
Hi, I am Frank
Hi, I am Frank
Hi, I am Frank
```
When we run the script we should get a different/new version as a response without much delay.
```
$ ./deploy.sh
$
$ for i in `seq 1 10`; do curl $DESTINATION; done
Hi, I am Frank
Hi, I am Frank
Hi, I am Frank
Hi, I am Frank
Hi, I am Carol
Hi, I am Carol
Hi, I am Carol
Hi, I am Carol
Hi, I am Carol
Hi, I am Carol
```

However we should also be able to rollback to the earlier setup.

#### Evaluation criteria
 - [x] The infrastructure setup is automated using terraform or cloudformation and the code is checked into a version control system.
 - [x] The project contains a `README.md` with documentation on how to run the project.

#### Deliverable
- [x] The source is accessible on a Github repository. (Using GitLab to show off my GitlabCI skills)
- [x] The infrastructure is publicly available and can be tested similar to the scripts above.
- [x] Provide us IAM credentials to login to the AWS account. If you have other resources in it make sure we can only access what is related to this test.

#### Bonus

- [x] We can manage traffic distribution with finer grained controls e.g. percent wise distribution like 20% `Frank` and 80% `Carol`
- [ x/x] (Kind of) The project has a CI/CD Pipeline with manual approval.

### AWS Services used and Architecture

The following AWS Services were used:
- IAM
- EC2 (Load Balancing)
- ECS
- ECR
- CodeDeploy
- CodePipeline
- S3

The architecture diagram is the following:

![](architecture.png)

### Requirements
- docker must be installed
- terraform must be installed
- aws cli must be installed

### How to deploy the system
It is as simple as changing the variables on the variables.tf file to your liking, specially the traffic control configurations whether you want to change them or not use traffic flow at all; and then doing "terraform apply" on the terraform directory. 

### How to update the deployment
Just set the variables and run ./deploy.sh, this will create the docker images and upload them to ECR; then it will start the blue green deployment. Another way to start the deployment is by using Gitlab as well. 

### How to test the deployment
Run ./test.sh while the deployment is running, you should see the app slowly changing over time. 

### Why the CI/CD pipeline kinda works
Creating the Codepipeline as said in https://stackoverflow.com/questions/57837028/the-deployment-specifies-that-the-revision-is-a-null-file-but-the-revision-prov  I had many internal errors referenced below, I have to call AWS support to resolve this. 

![](error_screenshot0.png)

![](error_screenshot1.png)

### Further improvements
- Create VPCs and Subnets instead of using the default ones

### Security issues that could be adressed
- Cloud Pipeline Policy too open
- ECS has public IP as to communicate with ECR

#### References
- https://docs.aws.amazon.com/codedeploy/latest/userguide/deployment-steps-ecs.html
- https://docs.aws.amazon.com/codedeploy/latest/userguide/tutorial-ecs-deployment.html
- https://medium.com/boltops/gentle-introduction-to-how-aws-ecs-works-with-example-tutorial-cea3d27ce63d
- https://aws.amazon.com/blogs/containers/aws-codedeploy-now-supports-linear-and-canary-deployments-for-amazon-ecs/
- https://stackoverflow.com/questions/67301268/aws-fargate-resourceinitializationerror-unable-to-pull-secrets-or-registry-auth
- https://stackoverflow.com/questions/61265108/aws-ecs-fargate-resourceinitializationerror-unable-to-pull-secrets-or-registry
- https://www.chakray.com/creating-fargate-ecs-task-aws-using-terraform/
- https://engineering.finleap.com/posts/2020-02-20-ecs-fargate-terraform/
- https://amanjeev.com/blog/aws-load-balancers-and-health-checks-with-terraform
